import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/views/Hello'
import PostsManager from '@/views/PostsManager'
import Test from '@/views/Test'
import Auth from '@okta/okta-vue'

Vue.use(Auth, {
  issuer: 'https://dev-478650.okta.com/oauth2/default',
  client_id: '0oa2sv7leo6LaOOf3357',
  redirect_uri: 'http://localhost:8080/implicit/callback',
  scope: 'openid profile email'
})

Vue.use(Router)

let router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello
    },
    {
      path: '/implicit/callback',
      component: Auth.handleCallback()
    },
    {
      path: '/posts-manager',
      name: 'PostsManager',
      component: PostsManager,
      meta: {
        requiresAuth: true
      }
    }, 
    {
      path: '/test', 
      name: 'Test', 
      component: Test, 
      meta: {
        requiresAuth: true
      }
    }
  ]
})

router.beforeEach(Vue.prototype.$auth.authRedirectGuard())

export default router